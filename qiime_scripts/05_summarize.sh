#!/bin/bash
#PBS -N sumqiime2
#PBS -o 98_log_files/qiime_summary.out
#PBS -l walltime=02:00:00
#PBS -l mem=60g
#####PBS -m ea 
#PBS -r n

cd $PBS_O_WORKDIR
# module load
. /appli/bioinfo/qiime/latest/env.sh

qiime feature-table summarize \
  --i-table /home1/datawork/alambard/microbiome_sacc/table.qza \
  --o-visualization /home1/datawork/alambard/microbiome_sacc/table.qzv \
  --m-sample-metadata-file /home1/datawork/alambard/microbiome_sacc/sample_metadata_capamax

qiime feature-table tabulate-seqs \
  --i-data /home1/datawork/alambard/microbiome_sacc/rep-seqs.qza \
  --o-visualization /home1/datawork/alambard/microbiome_sacc/rep-seqs.qzv
