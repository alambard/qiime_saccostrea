#!/bin/bash
#PBS -N sumqiime2
#PBS -o 98_log_files/qiime_diversity.out
#PBS -l walltime=02:00:00
#PBS -l mem=60g
#####PBS -m ea 
#PBS -r n

cd $PBS_O_WORKDIR

# module load
. /appli/bioinfo/qiime/latest/env.sh
# Prior check sampling depth in https://view.qiime2.org/

qiime diversity core-metrics-phylogenetic \
  --i-phylogeny /home1/datawork/alambard/microbiome_sacc/rooted-tree.qza \
  --i-table /home1/datawork/alambard//microbiome_sacc/table.qza \
  --p-sampling-depth 12000 \
  --m-metadata-file /home1/datawork/alambard//microbiome_sacc/sample_metadata_capamax \
  --output-dir core-metrics-results
