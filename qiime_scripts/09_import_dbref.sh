#!/bin/bash
#PBS -N sumqiime2
#PBS -o 98_log_files/qiime_importref.out
#PBS -l walltime=02:00:00
#PBS -l mem=60g
#####PBS -m ea 
#PBS -r n

cd $PBS_O_WORKDIR

# module load
. /appli/bioinfo/qiime/latest/env.sh

import ref.fasta + taxonomy
qiime tools import \
  --type 'FeatureData[Sequence]' \
  --input-path /home/ref-bioinfo/beedeem/n/silva/16S/99_otus.fasta \
  --output-path /home1/datawork/alambard/microbiome_sacc/ref-otus.qza

qiime tools import \
  --type 'FeatureData[Taxonomy]' \
  --input-format HeaderlessTSVTaxonomyFormat \
  --input-path /home/ref-bioinfo/beedeem/n/greengenes_13_8_otustax/download/greengenes_13_8_otustax/99_otu_taxonomy.txt \
  --output-path /home1/datawork/alambard/microbiome_sacc/ref-taxonomy.qza

#extract specific regions
qiime feature-classifier extract-reads \
  --i-sequences /home1/datawork/alambard/microbiome_sacc/ref-otus.qza \
  --p-f-primer GTGYCAGCMGCCGCGGTAA \
  --p-r-primer GGACTACNVGGGTWTCTAAT \
  --o-reads /home1/datawork/alambard/microbiome_sacc/ref-seq.qza

#classifier
qiime feature-classifier fit-classifier-naive-bayes \
  --i-reference-reads /home1/datawork/alambard/microbiome_sacc/ref-seq.qza \
  --i-reference-taxonomy /home1/datawork/alambard/microbiome_sacc/ref-taxonomy.qza \
  --o-classifier /home1/datawork/alambard/microbiome_sacc/classifier.qza

qiime metadata tabulate \
  --m-input-file /home1/datawork/alambard/microbiome_sacc/taxonomy.qza \
  --o-visualization /home1/datawork/alambard/microbiome_sacc/taxonomy.qzv

#visualization
qiime taxa barplot \
  --i-table /home1/datawork/alambard/microbiome_sacc/table.qza \
  --i-taxonomy /home1/datawork/alambard/microbiome_sacc/taxonomy.qza \
  --m-metadata-file /home1/datawork/alambard/microbiome_sacc/sample_metadata_capamax \
  --o-visualization /home1/datawork/alambard/microbiome_sacc/taxa-bar-plots.qzv

