#!/bin/bash
#PBS -N dada2qiime2
#PBS -o 98_log_files/qiime_dada2.out
#PBS -l walltime=02:00:00
#PBS -l mem=120g
#PBS -q omp
#PBS -l ncpus=15
#####PBS -m ea 
#PBS -r n

cd $PBS_O_WORKDIR

# module load
. /appli/bioinfo/qiime/latest/env.sh

qiime dada2 denoise-paired \
  --i-demultiplexed-seqs /home1/datawork/alambard/microbiome_sacc/paired-end-demux.qza \
  --o-table /home1/datawork/alambard/microbiome_sacc/table-dada2.qza \
  --o-representative-sequences /home1/datawork/alambard/microbiome_sacc/rep-seqs-dada2.qza \
  --o-denoising-stats /home1/datawork/alambard/microbiome_sacc/sample-dada-stats.qza \
  --p-n-threads 15 \
 --p-trim-left-f 13 \
  --p-trim-left-r 13 \
  --p-trunc-len-f 200 \
  --p-trunc-len-r 200

