#!/bin/bash
#PBS -N sumqiime2
#PBS -o 98_log_files/qiime_mafft.out
#PBS -l walltime=02:00:00
#PBS -l mem=60g
#####PBS -m ea 
#PBS -r n

cd $PBS_O_WORKDIR

# module load
. /appli/bioinfo/qiime/latest/env.sh

qiime alignment mafft \
  --i-sequences /home1/datawork/alambard/microbiome_sacc/rep-seqs.qza \
  --o-alignment /home1/datawork/alambard/microbiome_sacc/aligned-rep-seqs.qza

qiime alignment mask \
  --i-alignment /home1/datawork/alambard//microbiome_sacc/aligned-rep-seqs.qza \
  --o-masked-alignment /home1/datawork/alambard/microbiome_sacc/masked-aligned-rep-seqs.qza

qiime phylogeny fasttree \
  --i-alignment /home1/datawork/alambard/microbiome_sacc/masked-aligned-rep-seqs.qza \
  --o-tree /home1/datawork/alambard/microbiome_sacc/unrooted-tree.qza

qiime phylogeny midpoint-root \
  --i-tree /home1/datawork/alambard/microbiome_sacc/unrooted-tree.qza \
  --o-rooted-tree /home1/datawork/alambardS/microbiome_sacc/rooted-tree.qza
