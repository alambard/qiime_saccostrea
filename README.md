# Qiime_Saccostrea

The data presented here are the results of Ilumina metabarcoding sequencing on the rRNA
16S genes from 3 different tissues of Saccostrea sp with 44 samples.

Analysis of this data using QIIME2 , to detect and estimate the diversity of the animal's microbiota and check for the presence of pathogens
